/* 
 * Only the setup and loop functions exist in this tab
 * They compile second, and only exist to run the functions.
 * Due to the large amount of Millis() calculations, the loop is quite large.
 * So it makes sense to break the code into smaller chunks.
*/

// Start the LCD system
// Turn on the backlight
// Begin the LCD Start sequence
// Set all the timers to cur millis()
// Start the serial, at baud 9600
void setup()
{
  // Serial
  Serial.begin(9600);
  while(!Serial);
  // Wire
  Wire.begin();
  Serial.println("Initializing...");

  // Set Pin modes
  SetPinDesignations();
  Serial.println("Pin designation set");
  
  // LCD
  lcd.init();
  LcdStartSequence();
  Serial.println("LCD Setup Complete");

  // Baro
  SetupBaro();
  Serial.println("Barometer Setup");

  // Timers
  SynchroniseTime();
  Serial.println("Timers synchronised");

  // Eeprom Read
  curFanSpeed = TryGetFanSpeedValue();
  curTempSetPoint = TryGetTempAdjustment();
  Serial.println("Eeprom Variables Loaded");

  // Done
  Serial.println("Initialization Complete");
  delay(1500);  
  lcd.clear();
 }

// Main loop function, called every thread. 
// Gets the current milliseconds value
// Checks through the timers to see if the current milliseconds, minus the last time the 
// function was called, is more than the interval. If it is, the corresponding function
// is called, and the timer is reset. 
void loop()
{
  // Grab changeable variables
  long curMillis = millis(); 
  
  // Timer checks. 
  // Vacuum sensor - Takes a measurement from the vacuum sensor
  // Also pushes Web Server Updates
  if(curMillis - time_VacSense > time_VacSenseCall){ CheckVacuumSensor(); time_VacSense = millis(); client = server.available();   if (client) { printWEB(); } }
  
  // Input check - To see if buttons are pressed
  if(curMillis - time_InputCheck > time_InputCheckCall) { CheckInputSystem(); time_InputCheck = millis(); }
  
  // Backlight Timer - Turns off the backlight when it expires. 
  if(curMillis - time_BacklightCheck > time_BacklightCall) { ToggleBacklight(false);}

  // Baro Pressure Timer - Updates the current baromic pressure. 
  if(curMillis - time_PressureCheck > time_PressureCheckCall) { UpdateBaroPressure(); time_PressureCheck = millis(); SendOutput(); }

  if(!b_cannotConnect)
    if(curMillis - time_wifiCheck > time_wifiCheckCall) { CheckWifiStatus(); }
}
