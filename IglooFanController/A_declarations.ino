/* 
 *  All Variables, and includes, are defined within this tab. 
 *  They are compiled first, negating any variable missplacement issues.
*/
// Library includes
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>
#include <SPI.h>
#include <WiFiNINA.h>
#include <Arduino.h>
#include <Wire.h>
#include <BMP180I2C.h>

// Pin definitions
#define PWMOUT_PIN 9
#define PSIUP_PIN 4
#define PSIDOWN_PIN 5
#define OVERRIDE_PIN 7
#define VACIN_PIN A1 

#define I2C_ADDRESS 0x77

#define NO_OF_PSI_READINGS 10 // Amount of PSI readings for average

// MAP Sensor definitions
#define NUM_SAMPLES 20
#define MAP_MAX_PSI 1.45038
#define MAP_MIN_PSI 24.5114

// LCD Display 
// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Timing System - current millis cache
uint32_t time_VacSense = 0;
uint32_t time_InputCheck = 0;
uint32_t time_BacklightCheck = 0;
uint32_t time_PressureCheck = 0;
uint32_t time_timeout = 0;
uint32_t time_wifiCheck = 0;

// Timing System - milliseconds between each loop called.
float time_VacSenseCall = 16000;
float time_InputCheckCall = 200;
float time_BacklightCall = 6000;
float time_PressureCheckCall = 15000;
float time_timeoutCall = 10000;
float time_wifiCheckCall = 10000;


// God Tier Variables
double curPSI = 0;  // Current PSI, updated by CheckVacuumSensor()
double averagePSI = 0; // The average PSI, updated by CheckVacuumSensor()
double curPWM = 0;  // Current PWM, updated by SendOutput()
double targetPSI = 0; // Target PSI, set by CheckInputSystem() and TryGetTargetPSI
float currentBaroPressure = 14.982398; // Current baromic pressure. PSI. 
float currentAirTemperature = 0; // Current air temperature. DegC
float curTempAdjust = 0; // Adjustment value for calibration system. CurrentPressure & Baro Pressure diff
int curFanSpeed = 0; // Fan speed as a percentage 0-100
int curTempSetPoint = 14; // Set point at which the screen starts to get looser.

bool b_safetyOverride = false; // Overrides output to 0 duty pwm, displays a warning message. 
bool b_backlightON = false;
bool b_cannotConnect = false;

// Map sensor variables 
float sum = 0;               // sum of samples taken
int sample_count = 0;        // current sample number
float input_voltage = 4.90;  // measured with multimeter
double PSIreadings[NO_OF_PSI_READINGS];    // Number of PSI readings taken
int curPSIReading = 0;

// LCD Screen char buffers
char curRPM[5]; // RPM string to get 4 char buffer

// Wifi System
char ssid[] = "Igloo";     //  your network SSID (name)
char pass[] = "1glooVision";  // your network password
int wifiStatus = WL_IDLE_STATUS;     // the Wifi radio's status
int wifiKeyIndex = 0; // Network Key Index
WiFiServer server(80); // Web Server Socket
WiFiClient client = server.available(); // WebServer client
 
// Baro Pressure BMP180
//create an BMP180 object using the I2C interface
BMP180I2C bmp180(I2C_ADDRESS);
