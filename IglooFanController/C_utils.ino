 /*
   Functions that return a value are located here.
   These don't make sense in the normal funciton page, as they are
   often Utility features that do not require alteration once written.
*/

// Called from Setup
// Sets all the timers at the same time, with the same millis()
// stops calling millis(); on each line.
// moving it out into a seperate function shortens the setup loop.
void SynchroniseTime() 
{
  short curTime = millis();
  time_VacSense = curTime;
  time_InputCheck = curTime;
  time_BacklightCheck = curTime;
  time_PressureCheck = curTime;
  time_wifiCheck = curTime;
}

// Sets the sampling mode, and ensures the baro interface 
// is active and functioning. 
void SetupBaro(){
  if(!bmp180.begin()){
    Serial.println("Begin() failed. Check BMP180 interface");
    while(1);
  }
  // Reset sensor to default
  bmp180.resetToDefaults();

  // enabled ultra high resolution
  bmp180.setSamplingMode(BMP180MI::MODE_UHR);
}

// LCD Backlight Toggle
// Takes a bool value.
// A false value will turn OFF the LCD Backlight
// A True value will turn ON the LCD Backlight
void ToggleBacklight(bool b_On)
{
  if (b_On)
  {
    // Add more time
    time_BacklightCheck = millis();
    // If backlight is already on, no need to turn it on again
    if(b_backlightON)
    {
      return;
    }
    lcd.backlight();
    b_backlightON = true;
    Serial.println("Backlight: ON");
  }
  else
  {
    if(b_backlightON){
      lcd.noBacklight();
      Serial.println("Backlight: OFF");
      b_backlightON = false;
    }
  }
}

// Save PSI data to EEPROM, Arduino's tiny hard drive
// Uses the put function, which places the variable at location 0 (of 255)
// only updates the variable if it is changed, saving write calls which take 3.3ms
// Takes a float variable <newVal> and prints result to serial.
void SaveFanSpeedValue(float newVal)
{
  EEPROM.put(0, newVal);
  Serial.print("Saved Fan Speed Value: ");
  Serial.println(newVal, 2);
}


// Does a similar thing to SavePSIValue function.
void SaveTempAdjustment(float newVal){
  EEPROM.put(1, newVal);
  Serial.print("Saved Temp Adjust Value: ");
  Serial.println(newVal, 2);  
}

// Float function that returns the stored PSI value in Eeprom (if it exists)
// If no value is found, it will return 0.00f, instead of NAN. Which is a common issue
// when using the EEPROM.load() function.
float TryGetFanSpeedValue()
{
  float speed = 0.0f;
  EEPROM.get(0, speed);
  if(isnan(speed) || speed < -101.0f || speed > 101.0f )
  {
    speed = 1.0f;
  }
  Serial.print("Fan Speed: ");
  Serial.println(speed, 1);
  return speed;
}

// Float function that returns the stored PSI value in Eeprom (if it exists)
// If no value is found, it will return 0.00f, instead of NAN. Which is a common issue
// when using the EEPROM.load() function.
float TryGetTempAdjustment()
{
  float tempAdj = 0;
  EEPROM.get(1, tempAdj);
  if(isnan(tempAdj))
  {
    tempAdj = 0;
  }
  Serial.print("Target Temp Adjust: ");
  Serial.println(tempAdj, 1);
  return tempAdj;
}

// Updates the LCD display when called.
// In normal function: LCD will display the current PSI value, Target PSI value, Current PWM value
// In safety override mode: LCD will display an override warning message.
// bool <SafeMode> sets the functionality between regular update, and override message.
void UpdateLCDDisplay(bool SafeMode)
{
  if (!SafeMode) // Normal operation
  {
    lcd.setCursor(0, 0);
    lcd.print(String(curFanSpeed) + "%");
    lcd.setCursor(12, 0);
    lcd.print(CalculateVoltageOutput(), 1);
    lcd.setCursor(0, 1);
    lcd.print(String(currentAirTemperature));
    lcd.setCursor(12, 1);
    sprintf(curRPM, "%04d", CalculateRPM());
    lcd.print(curRPM);
  }
  else // Safety operation
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Safety Override");
    lcd.setCursor(0, 1);
    lcd.print("PWM Set to zero");
  }
}

void UpdateBaroPressure(){
  //start a temperature measurement
	if (!bmp180.measureTemperature())
	{
		Serial.println("could not start temperature measurement, is a measurement already running?");
		return 0.0;
	}

	//wait for the measurement to finish. proceed as soon as hasValue() returned true. 
	do
	{
		delay(100);
	} while (!bmp180.hasValue());

  currentAirTemperature = bmp180.getTemperature();

	Serial.print("Air Temperature: "); 
	Serial.print(currentAirTemperature); 
	Serial.println(" degC");

  //start a pressure measurement. 
	if (!bmp180.measurePressure())
	{
		Serial.println("could not start perssure measurement, is a measurement already running?");
		return;
	}

	//wait for the measurement to finish. proceed as soon as hasValue() returned true. 
	do
	{
		delay(100);
	} while (!bmp180.hasValue());

  currentBaroPressure = bmp180.getPressure() * 0.000145038;

	Serial.print("Pressure: "); 
	Serial.print(currentBaroPressure);
	Serial.println(" PSI");


}

void CheckSafetyOverridePin()
{
  // Safety Override Pin
  if (digitalRead(OVERRIDE_PIN) == LOW)
  {
    // Safety Override Enabled
    if (!b_safetyOverride) // Single loop before setting bool
    {
      Serial.println("Safety Override Enabled");
      // Turn on the LCD Screen backlight
      ToggleBacklight(true);
      // Set the LCD Screen into safety mode
      UpdateLCDDisplay(true);
      // Set the bool
      b_safetyOverride = true;
    }
  }
  else
  {
    if (b_safetyOverride)
    {
      Serial.println("Safety Override Disabled");
      // Turn on the LCD Screen backlight
      ToggleBacklight(true);
      // Set the LCD Screen into safety mode
      lcd.clear();
      UpdateLCDDisplay(false);
      // Safety Override Disabled
      b_safetyOverride = false;
    }
  }
}

// Constrains the fan speed between 0-100
int UpdateFanSpeed(int newFanSpeed){
  int constrainedSpeed = constrain(newFanSpeed, 0, 100);
  SaveFanSpeedValue(constrainedSpeed);
  return constrainedSpeed;
}

 int CalculatePWM(int newFanSpeed){
   int val = (int)mapFloat(newFanSpeed, 0, 100, 0, 255);
   curPWM = val;
   return val;
 }

// used only for an extra thing to fill up the display
// takes the curPWM value, and maps it to the theoretical
// max rpm of the fan.
int CalculateRPM()
{
  return mapFloat(curPWM, 0, 255, 0, 2800);
}

double GetAveragePSI(){

  // Add cur PSI to the array, in the current slot
  PSIreadings[curPSIReading] = curPSI;
  // advance the PSI reading slot
  curPSIReading++;
  // Check if we loop the reading. 
  if(curPSIReading > NO_OF_PSI_READINGS) curPSIReading = 0;
  
  double total = 0.0; 
  for(int i = 0; i < NO_OF_PSI_READINGS; i++){
    total += PSIreadings[i];
  }
  return total / (double)NO_OF_PSI_READINGS;
}

// Calculates the voltage output to the van
// by mapping the PWM to between 0 and 10 volts.
float CalculateVoltageOutput(){
  return mapFloat(curPWM, 0, 255, 0, 10);
}

// Custom version of the Arudino inbuilt map system.
// Takes a value between one min/max, and converts it to the comparable value between another min/max
float mapFloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void printWifiStatus() {
  // print the SSID of the network
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");

  Serial.print("Web Server active at IP: http://");
  Serial.print(ip);
  Serial.println(":80");

  ToggleBacklight(true);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("IP Address:");
  lcd.setCursor(0,1);
  String ipst = toString(ip) + ":80";
  lcd.print(ipst);
  delay(3000);
  lcd.clear();
}

String toString(const IPAddress& address){
  return String() + address[0] + "." + address[1] + "." + address[2] + "." + address[3];
}

void printMacAddress(byte mac[]) {
  for (int i = 5; i >= 0; i--) {
    if (mac[i] < 16) {
      Serial.print("0");
    }
    Serial.print(mac[i], HEX);
    if (i > 0) {
      Serial.print(":");
    }
  }
  Serial.println();
}
