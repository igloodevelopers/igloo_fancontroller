/* 
 * Any functions called by the setup or loop main functiosn
 * are executed here. This helps to organise the code and locate 
 * specific funtions easier. Rather than a large 1000 line page.
*/

// Called from Setup()
// Prints a startup message when called. 
// Sets the cursor position to line 0, position 0, prints: "Smart Fan"
// moves the cursor to line 1, position 0, prints: "Starting up"
void LcdStartSequence()
{
  ToggleBacklight(true);
  lcd.setCursor(0,0);
  lcd.print("Smart Fan");
  lcd.setCursor(0,1);
  lcd.print("Starting Up");
}

// Called from Setup()
// Runs through each pin that is defined and set it's pin designation
// Pullup pins are grounded when the switch is activated
void SetPinDesignations()
{
  // Pullup pins sense when they are grounded
  pinMode(PSIUP_PIN, INPUT_PULLUP); 
  pinMode(PSIDOWN_PIN, INPUT_PULLUP); 
  pinMode(OVERRIDE_PIN, INPUT_PULLUP); 
  // Input pin provides analog 0-5v from sensor
  pinMode(VACIN_PIN, INPUT);  
  // Output pin for PWM
  pinMode(PWMOUT_PIN, OUTPUT);

  analogReference(EXTERNAL);
}

// Called from Loop() upon time_CheckVac interval
// Performs 10 checks on the sensors output, and then aggregates the results
// Calculates the PSI based on the pin voltage and sensor tolerances. 
// Sets the curPSI Value
void CheckVacuumSensor()
{
  if(!b_safetyOverride) // Normal operation
  {
  // Take measurements based on NUM_SAMPLE Define
  sum = 0; 
  sample_count = 0;
  while(sample_count < NUM_SAMPLES) {
    sum += float(analogRead(VACIN_PIN));
    sample_count++;
    delay(1);
  }
  sum = sum / sample_count;
  
  // Convert to voltage
  float voltage = sum * (5.0  / 1023.0); // Returns voltage between 0 - Input_Voltage

  // Interpret Voltage
  if(voltage < 0.3){
    Serial.println("Open Circuit, check connections");
  }
  else if(voltage < 5)
  {
    float refPSI = mapFloat(voltage, 0.0,  5.0, MAP_MIN_PSI, MAP_MAX_PSI);

    curPSI = refPSI - currentBaroPressure ;
    
    Serial.print("Current PSI: ");
    Serial.println(curPSI);
  }
  }
  else // Safety Override - No need for calculations
  {
    return; 
  }
  
}

void CheckWifiStatus()
{  
  // Check Wifi
  if(wifiStatus != WL_CONNECTED)
  {  
    // check for the WiFi module:
    if (WiFi.status() == WL_NO_MODULE) {
      Serial.println("Communication with WiFi module failed!");
      // don't continue
      return;
    }

    // Check Wifi Firmware
    if (WiFi.firmwareVersion() < WIFI_FIRMWARE_LATEST_VERSION) {
      Serial.println("Please upgrade the firmware");
    }

    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    wifiStatus = WiFi.begin(ssid, pass);
    
    delay(1500);
    if(wifiStatus == WL_CONNECTED){
        printWifiStatus();
        server.begin();      
    }
    else{
      b_cannotConnect = true;
        ToggleBacklight(true);
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Could not connect");
        lcd.setCursor(0,1);
        lcd.print("To Wifi");
        delay(1500);
        lcd.clear();
    }

  }
}

// Called from Loop() upon time_CheckInput interval
// Checks to see if any button input pins are grounded
// If the nudge buttons are pressed it will either adjust the current PSI up or down
// If the override button is toggled. Output PWM will be overriden at 0, as an emergency
// fan shut off (as unplugging the controller will cause the fan to ramp up to 100%
void CheckInputSystem()
{
  // Check the safety pin.
  CheckSafetyOverridePin();
  // If we are in safe mode, don't bother checking the other buttons
  if(b_safetyOverride) return; 
  int newFanSpeed = curFanSpeed;
  // Check PSIUP pin
  if(digitalRead(PSIUP_PIN) == LOW)
  {
    // Turn on the LCD Screen backlight
    ToggleBacklight(true);
    
    curFanSpeed = UpdateFanSpeed(newFanSpeed += 1);
    
    // Update the LCD Screen (not in safe mode)
    UpdateLCDDisplay(false);
    
  }
  // Check PSI Down pin, 'else' so you can't press both.
  else if(digitalRead(PSIDOWN_PIN) == LOW) 
  {
    // Turn on the LCD Screen backlight
    ToggleBacklight(true);

    curFanSpeed = UpdateFanSpeed(newFanSpeed -= 1);
    
    // Update the LCD Screen (not in safe mode)
    UpdateLCDDisplay(false);
  }
}

// Called from Loop() upon time_SendOutput interval
// Calculates the required PWM from the input data, and the current
// vacuum data and sends it out via the PWM pin to the fan controller.
// If the b_safetyOverride variable is True, all calculations are skipped, and pwm duty is set to 0.
// This is outout to the VNH2SP30 board which translates the PWM signal into a DC analog 0-10v
void SendOutput()
{
  if(b_safetyOverride) // Safety operation - PWM duty overriden to 0
  {
    analogWrite(PWMOUT_PIN, 0); 

  }
  else // Normal operation - PWM duty between 0 - 255;
  {
    if(currentAirTemperature > curTempSetPoint){
      int diff = (int)currentAirTemperature - curTempSetPoint;
      if(diff > 5) {
        curTempAdjust = 5;
        Serial.print(diff);
        Serial.println(" difference in Temperature");
        }
      else if(diff > 3){
        curTempAdjust = 3;
      } 
      else {
        curTempAdjust = 0;
      }
    }

    analogWrite(PWMOUT_PIN, CalculatePWM(curFanSpeed + curTempAdjust)); 
      // Update the LCD Screen (not in safe mode)
    UpdateLCDDisplay(false);
  }
}

void printWEB()
{
  if (client) {                             // if you get a client,
    Serial.println("new client");           // print a message out the serial port
    String currentLine = "";                // make a String to hold incoming data from the client

    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();        

            // Print the Data
            client.print("<h1> IGLOO SMART FAN </h1><br>");
            client.print("<b>Current Operating Data</b><br><br>");
            // Current Air Pressure
            client.print("Ambient Air Pressure :");
            client.print(currentBaroPressure);
            client.print(" psi<br>");
            // Current Air Temperature
            client.print("Ambient Air Temperature :");
            client.print(currentAirTemperature);
            client.print(" degrees C<br>");
            // Last Sensor Read
            client.print("Last Pressure Sensor Value :");
            client.print(curPSI);
            client.print(" psi<br>");
            // Current Target PSI
            client.print("Current Fan Speed :");
            client.print(curFanSpeed);
            client.print("%<br><br>");

            client.print("<b>Screen Temperature Settings</b><br><br>");
            // Current percentage adjustment
            client.print("Temp adjustment value : ");
            client.print(String(curTempAdjust));
            client.print("*C<br>");

            // Current Temperature Set Point
            client.print("Temp set point : ");
            client.print(String(curTempSetPoint));
            client.print("*C<br><br>");

            //create the buttons
            client.print("Click <a href=\"/H1\">HERE</a> to increase set temperature 1<br>");
            client.print("Click <a href=\"/H2\">HERE</a> to increase set temperature 10<br><br>");
            client.print("Click <a href=\"/L1\">HERE</a>  to decrease set temperature 1<br>");
            client.print("Click <a href=\"/L2\">HERE</a>  to decrease set temperature 10<br><br>");

            // The HTTP response ends with another blank line:
            client.println();
            // break out of the while loop:
            break;
          }
          else {      // if you got a newline, then clear currentLine:
            currentLine = "";
          }
        }
        else if (c != '\r') {    // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }

        if (currentLine.endsWith("GET /H1")) {
          curTempSetPoint += 1;   
          SaveTempAdjustment(curTempSetPoint);    
        }

        if (currentLine.endsWith("GET /L1")) {
          curTempSetPoint -= 1;     
          SaveTempAdjustment(curTempSetPoint); 
        }

        if (currentLine.endsWith("GET /H2")) {
          curTempSetPoint += 10;     
          SaveTempAdjustment(curTempSetPoint); 
        }
        
        if (currentLine.endsWith("GET /L2")) {
          curTempSetPoint -= 10;     
          SaveTempAdjustment(curTempSetPoint); 
        }
      }
    }
    // close the connection:
    client.stop();
  }
}
